package serio

import (
	"bytes"
	"io"
	"testing"
	"time"
)

type testbuf struct {
	rdr   io.Reader
	delay time.Duration
	count int
	limit int
}

func (b *testbuf) Read(p []byte) (int, error) {
	b.count++
	if b.count == 2 {
		return 0, io.EOF
	}
	time.Sleep(b.delay)
	return b.rdr.Read(p)
}

func TestTimeout(t *testing.T) {
	resp := []byte("Hello world\r\n")
	buf := testbuf{
		rdr:   bytes.NewReader(resp),
		delay: time.Second * 3,
		limit: 4,
	}

	rdr := NewTimeoutReader(&buf, time.Second*2)
	p := make([]byte, len(resp))
	_, err := rdr.Read(p)
	_, ok := err.(*TimeoutError)
	if !ok {
		t.Fatalf("Unexpected error: %v", err)
	}
}

func TestStreamTimeout(t *testing.T) {
	resp := []byte("Hello world\r\n")
	buf := testbuf{
		rdr:   bytes.NewReader(resp),
		delay: time.Second,
		limit: 4,
	}

	rdr := NewTimeoutReader(&buf, time.Second*2)
	p, err := rdr.ReadBytes('\n')

	_, ok := err.(*TimeoutError)
	if !ok {
		t.Fatalf("Unexpected error: %v", err)
	}

	if len(p) == 0 {
		t.Fatalf("No data read")
	}
}

func TestNoTimeout(t *testing.T) {
	resp := []byte("Hello world\r\n")
	buf := testbuf{
		rdr:   bytes.NewReader(resp),
		delay: time.Millisecond * 3,
		limit: 4,
	}

	rdr := NewTimeoutReader(&buf, time.Second*3)
	p := make([]byte, len(resp))
	_, err := rdr.Read(p)
	if err != nil {
		t.Fatalf("Read error: %v", err)
	}
	if bytes.Compare(p, resp) != 0 {
		t.Fatalf("Contents don't match: %q != %q", p, resp)
	}
}

func TestPartialRead(t *testing.T) {
	resp := []byte("Hello world\r\n")
	buf := testbuf{
		rdr:   bytes.NewReader(resp),
		delay: time.Millisecond * 3,
		limit: 4,
	}

	rdr := NewTimeoutReader(&buf, time.Second*3)
	p := make([]byte, 3)
	_, err := rdr.Read(p)
	if err != nil {
		t.Fatalf("Read error: %v", err)
	}
	if bytes.Compare(p, resp[0:3]) != 0 {
		t.Fatalf("Contents don't match: %q != %q", p, resp)
	}
}
