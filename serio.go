// Package serio provide various utility functions and types
// for working with serial ports.
package serio

import (
	"io"
	"time"
)

// io.Reader with a time limit
type TimeoutReader struct {
	input chan byte
	timeo time.Duration
	halt  chan bool
	rdr   io.Reader
}

// Error trigger by a read timeout
type TimeoutError struct {
	msg string
}

func (e *TimeoutError) Error() string {
	return e.msg
}

// NewTimeoutReader returns a new TimeoutReader with a read time limit
// set to timeo.
func NewTimeoutReader(rdr io.Reader, timeo time.Duration) *TimeoutReader {
	if timeo <= time.Duration(0) {
		return nil
	}

	input := make(chan byte, 32)
	halt := make(chan bool)

	go func() {
		var (
			n int
		)
		defer close(input)
		buf := make([]byte, 1)
		for {
			n, _ = rdr.Read(buf)
			select {
			case <-halt:
				return
			default:
				if n == 1 {
					input <- buf[0]
				}
			}
		}
	}()

	return &TimeoutReader{rdr: rdr, timeo: timeo, input: input, halt: halt}
}

// Read up to len(p) bytes into p. Returns the number of bytes read
// and any error that occurs.
func (tr *TimeoutReader) Read(p []byte) (int, error) {
	var err error
	var opened bool

	count := int(0)
loop:
	for i := 0; i < len(p); i++ {
		select {
		case p[i], opened = <-tr.input:
			if !opened {
				err = io.EOF
				break loop
			}
			count++
		case <-time.After(tr.timeo):
			err = &TimeoutError{msg: "Timeout expired"}
			break loop
		}
	}
	return count, err
}

// Read up to the first occurence of delim in the input. Returns the bytes
// read along with any error that occurs.
func (tr *TimeoutReader) ReadBytes(delim byte) ([]byte, error) {
	buf := make([]byte, 0)
	var b byte
	var err error
	var opened bool

	tm := time.NewTimer(tr.timeo)
	defer tm.Stop()
loop:
	for {
		select {
		case b, opened = <-tr.input:
			if !opened {
				err = io.EOF
				break loop
			}
			buf = append(buf, b)
			if b == delim {
				break loop
			} else {
				select {
				case <-tm.C:
					err = &TimeoutError{msg: "Timeout expired"}
					break loop
				default:
				}
			}
		case <-tm.C:
			err = &TimeoutError{msg: "Timeout expired"}
			break loop
		}
	}

	return buf, err
}

// Set the read timeout
func (tr *TimeoutReader) SetTimeout(t time.Duration) time.Duration {
	timeo := tr.timeo
	if t > time.Duration(0) {
		tr.timeo = t
	}
	return timeo
}

// Close the reader
func (tr *TimeoutReader) Close() error {
	close(tr.halt)
	return nil
}
